// Copyright 2020 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "RepObject.generated.h"

UCLASS(Abstract, Blueprintable)
class UNREALFEST_API URepObject : public UObject
{
    GENERATED_BODY()

public:
    virtual UWorld* GetWorld() const override;

    virtual bool IsNameStableForNetworking() const override;
    virtual bool IsSupportedForNetworking() const override;

    virtual int32 GetFunctionCallspace(UFunction* Function, FFrame* Stack) override;
    virtual bool CallRemoteFunction(UFunction* Function, void* Params, FOutParmRec* OutParams, FFrame* Stack) override;

private:
    UPROPERTY()
    uint8 bNetAddressable : 1;

public:
    void SetNetAddressable();
};
