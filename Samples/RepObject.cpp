// Copyright 2020 2Bit Studios, All Rights Reserved.

#include "RepObject.h"

#include "Net/UnrealNetwork.h"

UWorld* URepObject::GetWorld() const
{
    if (UObject const* const Outer = this->GetOuter())
    {
        // Hack to allow ImplementsGetWorld to return true in the blueprint designer
        // As this is a replicated object, we know we'll have a world at runtime
        if (Outer->GetClass() != UPackage::StaticClass())
        {
            return Outer->GetWorld();
        }
    }

    return nullptr;
}

bool URepObject::IsNameStableForNetworking() const
{
    return this->bNetAddressable || Super::IsNameStableForNetworking();
}

bool URepObject::IsSupportedForNetworking() const
{
    return true;
}

int32 URepObject::GetFunctionCallspace(UFunction* Function, FFrame* Stack)
{
    // Bubble up to outer (must be an actor/component/repobject)
    return this->GetOuter() != nullptr
        ? this->GetOuter()->GetFunctionCallspace(Function, Stack)
        : FunctionCallspace::Local;
}

bool URepObject::CallRemoteFunction(UFunction* Function, void* Params, FOutParmRec* OutParams, FFrame* Stack)
{
    for (UObject* Outer = this->GetOuter(); Outer != nullptr; Outer = Outer->GetOuter())
    {
        if (AActor* const OuterActor = Cast<AActor>(Outer))
        {
            if (UNetDriver* const NetDriver = OuterActor->GetNetDriver())
            {
                NetDriver->ProcessRemoteFunction(OuterActor, Function, Params, OutParams, Stack, this);

                return true;
            }

            break;
        }
    }

    return false;
}

void URepObject::SetNetAddressable()
{
    this->bNetAddressable = true;
}