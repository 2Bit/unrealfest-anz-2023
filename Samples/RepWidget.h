// Copyright 2020 2Bit Studios, All Rights Reserved.

#pragma once

#include "Blueprint/UserWidget.h"

#include "RepWidget.generated.h"

UCLASS(Abstract)
class UNREALFEST_API URepWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    virtual UWorld* GetWorld() const override;

    virtual bool IsNameStableForNetworking() const override;
    virtual bool IsSupportedForNetworking() const override;

    virtual int32 GetFunctionCallspace(UFunction* Function, FFrame* Stack) override;
    virtual bool CallRemoteFunction(UFunction* Function, void* Params, FOutParmRec* OutParams, FFrame* Stack) override;

private:
    UPROPERTY()
    uint8 bNetAddressable : 1;

    UFUNCTION(BlueprintCallable, Category = "Widget", meta = (DeterminesOutputType = "Class"))
    static URepWidget* CreateReplicatedWidget(APlayerController* Player, TSubclassOf<URepWidget> Class, FName Name = NAME_None, bool bMakeNetAddressable = true);

public:
    template <typename TRepWidget>
    static TRepWidget* Create(APlayerController* Player, TSubclassOf<TRepWidget> const& Class = TRepWidget::StaticClass(), FName const& Name = NAME_None, bool bMakeNetAddressable = true)
    {
        return (TRepWidget*)URepWidget::CreateReplicatedWidget(Player, Class, Name, bMakeNetAddressable);
    }
};

template <typename TRepWidget>
static TRepWidget* CreateReplicatedWidget(APlayerController* Player, TSubclassOf<TRepWidget> const& Class = TRepWidget::StaticClass(), FName const& Name = NAME_None, bool bMakeNetAddressable = true)
{
    return URepWidget::Create<TRepWidget>(Player, Class, Name, bMakeNetAddressable);
}
