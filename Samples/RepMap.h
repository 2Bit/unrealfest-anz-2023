// Copyright 2020 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "RepMap.generated.h"

USTRUCT()
struct FRepMapEntry : public FFastArraySerializerItem
{
    GENERATED_BODY()

public:
    void CopyReplicationHistory(FRepMapEntry const& From)
    {
        this->ReplicationID = From.ReplicationID;
        this->ReplicationKey = From.ReplicationKey;
        this->MostRecentArrayReplicationKey = From.MostRecentArrayReplicationKey;
    }
};

#define IMPLEMENT_REPMAPENTRY(KEY_PROPERTY)                                                         \
public:                                                                                             \
    FORCEINLINE decltype(KEY_PROPERTY) const& GetKey() const { return this->KEY_PROPERTY; }         \
    void SetKey(decltype(KEY_PROPERTY) const& NewKey)                                               \
    {                                                                                               \
        this->KEY_PROPERTY = NewKey;                                                                \
    }

USTRUCT()
struct FRepMap : public FFastArraySerializer
{
    GENERATED_BODY()

protected:
    FHashTable Hash;
    uint8 bNeedsRehash : 1;

public:
    FRepMap()
        : bNeedsRehash(true)
    {
    }

    FORCEINLINE void Rehash()
    {
        this->bNeedsRehash = true;
    }
};

#define IMPLEMENT_REPMAP(SELF_TYPE, VALUES_PROPERTY)                                                                  \
private:                                                                                                              \
    template <typename TPointer>                                                                                      \
    using TRemoveConst = std::add_pointer_t<std::remove_const_t<std::remove_pointer_t<TPointer>>>;                    \
                                                                                                                      \
public:                                                                                                               \
    using TValue = decltype(VALUES_PROPERTY)::ElementType;                                                            \
    using TKey = std::invoke_result_t<decltype(&TValue::GetKey), TValue>;                                             \
                                                                                                                      \
private:                                                                                                              \
    TArray<TValue> ShadowItems;                                                                                       \
                                                                                                                      \
public:                                                                                                               \
    DECLARE_DELEGATE_OneParam(FOnRepRemove, TValue const&  /*OldState*/);                                             \
    FOnRepRemove OnRepRemove;                                                                                         \
                                                                                                                      \
    DECLARE_DELEGATE_OneParam(FOnRepAdd, TValue const&  /*NewState*/);                                                \
    FOnRepAdd OnRepAdd;                                                                                               \
                                                                                                                      \
    DECLARE_DELEGATE_TwoParams(FOnRepChange, TValue const&  /*OldState*/, TValue const&  /*NewState*/);               \
    FOnRepChange OnRepChange;                                                                                         \
                                                                                                                      \
    void PreReplicatedRemove(TArrayView<int32> const& RemovedIndices, int32 FinalSize) const                          \
    {                                                                                                                 \
        const_cast<TRemoveConst<decltype(this)>>(this)->Rehash();                                                     \
                                                                                                                      \
        for (int32 const& Index : RemovedIndices)                                                                     \
        {                                                                                                             \
            this->OnRepRemove.ExecuteIfBound(this->ShadowItems[Index]);                                               \
        }                                                                                                             \
    }                                                                                                                 \
                                                                                                                      \
    void PostReplicatedAdd(TArrayView<int32> const& AddedIndices, int32 FinalSize) const                              \
    {                                                                                                                 \
        const_cast<TRemoveConst<decltype(this)>>(this)->Rehash();                                                     \
                                                                                                                      \
        for (int32 const& Index : AddedIndices)                                                                       \
        {                                                                                                             \
            this->OnRepAdd.ExecuteIfBound(this->VALUES_PROPERTY[Index]);                                              \
        }                                                                                                             \
    }                                                                                                                 \
                                                                                                                      \
    void PostReplicatedChange(TArrayView<int32> const& ChangedIndices, int32 FinalSize) const                         \
    {                                                                                                                 \
        for (int32 const& Index : ChangedIndices)                                                                     \
        {                                                                                                             \
            if (this->ShadowItems[Index].GetKey() != this->VALUES_PROPERTY[Index].GetKey())                           \
            {                                                                                                         \
                const_cast<TRemoveConst<decltype(this)>>(this)->Rehash();                                             \
            }                                                                                                         \
                                                                                                                      \
            this->OnRepChange.ExecuteIfBound(this->ShadowItems[Index], this->VALUES_PROPERTY[Index]);                 \
        }                                                                                                             \
    }                                                                                                                 \
                                                                                                                      \
    bool NetDeltaSerialize(FNetDeltaSerializeInfo &DeltaParams)                                                       \
    {                                                                                                                 \
        if (DeltaParams.Reader != nullptr)                                                                            \
        {                                                                                                             \
            this->ShadowItems = this->VALUES_PROPERTY;                                                                \
        }                                                                                                             \
                                                                                                                      \
        bool const Result = FFastArraySerializer::FastArrayDeltaSerialize(this->VALUES_PROPERTY, DeltaParams, *this); \
                                                                                                                      \
        this->ShadowItems.Empty();                                                                                    \
                                                                                                                      \
        return Result;                                                                                                \
    }                                                                                                                 \
                                                                                                                      \
    FORCEINLINE TArray<TValue> const& GetValues() const                                                               \
    {                                                                                                                 \
        return this->VALUES_PROPERTY;                                                                                 \
    }                                                                                                                 \
                                                                                                                      \
    FORCEINLINE int32 Num() const                                                                                     \
    {                                                                                                                 \
        return this->VALUES_PROPERTY.Num();                                                                           \
    }                                                                                                                 \
                                                                                                                      \
    int32 Add(TKey const& NewKey, TValue const& NewValue)                                                             \
    {                                                                                                                 \
        this->ConditionalRehash();                                                                                    \
                                                                                                                      \
        int32 Index = this->FindIndex(NewKey);                                                                        \
        if (Index < 0)                                                                                                \
        {                                                                                                             \
            Index = this->VALUES_PROPERTY.Add(NewValue);                                                              \
                                                                                                                      \
            this->Hash.Add(GetTypeHash(NewKey), Index);                                                               \
        }                                                                                                             \
        else                                                                                                          \
        {                                                                                                             \
            FRepMapEntry ReplicationHistory;                                                                          \
            ReplicationHistory.CopyReplicationHistory(this->VALUES_PROPERTY[Index]);                                  \
                                                                                                                      \
            this->VALUES_PROPERTY[Index] = NewValue;                                                                  \
            this->VALUES_PROPERTY[Index].CopyReplicationHistory(ReplicationHistory);                                  \
        }                                                                                                             \
                                                                                                                      \
        this->VALUES_PROPERTY[Index].SetKey(NewKey);                                                                  \
        this->MarkItemDirty(this->VALUES_PROPERTY[Index]);                                                            \
                                                                                                                      \
        return Index;                                                                                                 \
    }                                                                                                                 \
                                                                                                                      \
    void RemoveAt(int32 Index)                                                                                        \
    {                                                                                                                 \
        if (Index >= 0 && Index < this->Num())                                                                        \
        {                                                                                                             \
            this->ConditionalRehash();                                                                                \
                                                                                                                      \
            this->Hash.Remove(GetTypeHash(this->VALUES_PROPERTY[Index].GetKey()), Index);                             \
                                                                                                                      \
            this->VALUES_PROPERTY.RemoveAt(Index);                                                                    \
                                                                                                                      \
            this->MarkArrayDirty();                                                                                   \
        }                                                                                                             \
    }                                                                                                                 \
                                                                                                                      \
    FORCEINLINE void Remove(TKey const& Key)                                                                          \
    {                                                                                                                 \
        this->RemoveAt(this->FindIndex(Key));                                                                         \
    }                                                                                                                 \
                                                                                                                      \
    FORCEINLINE bool Contains(TKey const& Key) const                                                                  \
    {                                                                                                                 \
        return this->FindIndex(Key) >= 0;                                                                             \
    }                                                                                                                 \
                                                                                                                      \
    int32 FindIndex(TKey const& Key) const                                                                            \
    {                                                                                                                 \
        this->ConditionalRehash();                                                                                    \
                                                                                                                      \
        for (int32 i = this->Hash.First(GetTypeHash(Key)); this->Hash.IsValid(i); i = this->Hash.Next(i))             \
        {                                                                                                             \
            if (this->VALUES_PROPERTY[i].GetKey() == Key)                                                             \
            {                                                                                                         \
                return i;                                                                                             \
            }                                                                                                         \
        }                                                                                                             \
                                                                                                                      \
        return -1;                                                                                                    \
    }                                                                                                                 \
                                                                                                                      \
    TValue* Find(TKey const& Key)                                                                                     \
    {                                                                                                                 \
        int32 const Index = this->FindIndex(Key);                                                                     \
        if (Index >= 0 && Index < this->Num())                                                                        \
        {                                                                                                             \
            return &this->VALUES_PROPERTY[Index];                                                                     \
        }                                                                                                             \
                                                                                                                      \
        return nullptr;                                                                                               \
    }                                                                                                                 \
                                                                                                                      \
    TValue const* Find(TKey const& Key) const                                                                         \
    {                                                                                                                 \
        return const_cast<TRemoveConst<decltype(this)>>(this)->Find(Key);                                             \
    }                                                                                                                 \
                                                                                                                      \
    TValue const& operator[](TKey const& Key) const                                                                   \
    {                                                                                                                 \
        TValue const* const Result = this->Find(Key);                                                                 \
        check(Result != nullptr);                                                                                     \
        return *Result;                                                                                               \
    }                                                                                                                 \
                                                                                                                      \
    void Empty()                                                                                                      \
    {                                                                                                                 \
        this->VALUES_PROPERTY.Empty();                                                                                \
        this->Hash.Clear();                                                                                           \
    }                                                                                                                 \
                                                                                                                      \
private:                                                                                                              \
    FORCEINLINE void ConditionalRehash() const                                                                        \
    {                                                                                                                 \
        if (this->bNeedsRehash)                                                                                       \
        {                                                                                                             \
            auto const MutableThis = const_cast<TRemoveConst<decltype(this)>>(this);                                  \
                                                                                                                      \
            MutableThis->Hash.Clear();                                                                                \
                                                                                                                      \
            for (int i = 0; i < this->VALUES_PROPERTY.Num(); ++i)                                                     \
            {                                                                                                         \
                uint32 const Key = GetTypeHash(this->VALUES_PROPERTY[i].GetKey());                                    \
                                                                                                                      \
                MutableThis->Hash.Add(Key, i);                                                                        \
            }                                                                                                         \
                                                                                                                      \
            MutableThis->bNeedsRehash = false;                                                                        \
        }                                                                                                             \
    }                                                                                                                 \
                                                                                                                      \
public:                                                                                                               \
    auto begin()                                                                                                      \
    {                                                                                                                 \
        return this->VALUES_PROPERTY.begin();                                                                         \
    }                                                                                                                 \
                                                                                                                      \
    auto begin() const                                                                                                \
    {                                                                                                                 \
        return this->VALUES_PROPERTY.begin();                                                                         \
    }                                                                                                                 \
                                                                                                                      \
    auto end()                                                                                                        \
    {                                                                                                                 \
        return this->VALUES_PROPERTY.end();                                                                           \
    }                                                                                                                 \
                                                                                                                      \
    auto end() const                                                                                                  \
    {                                                                                                                 \
        return this->VALUES_PROPERTY.end();                                                                           \
    }                                                                                                                 \
};                                                                                                                    \
                                                                                                                      \
template <>                                                                                                           \
struct TStructOpsTypeTraits<SELF_TYPE> : public TStructOpsTypeTraitsBase2<SELF_TYPE>                                  \
{                                                                                                                     \
    enum                                                                                                              \
    {                                                                                                                 \
        WithNetDeltaSerializer = true,                                                                                \
    };
