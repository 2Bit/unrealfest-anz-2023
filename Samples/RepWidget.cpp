// Copyright 2020 2Bit Studios, All Rights Reserved.

#include "RepWidget.h"

#include "Blueprint/WidgetTree.h"

UWorld* URepWidget::GetWorld() const
{
    if (UObject const* const Outer = this->GetOuter())
    {
        // Hack to allow ImplementsGetWorld to return true in the blueprint designer
        // As this is a replicated object, we know we'll have a world at runtime
        if (Outer->GetClass() != UPackage::StaticClass())
        {
            return Outer->GetWorld();
        }
    }

    return nullptr;
}

bool URepWidget::IsNameStableForNetworking() const
{
    return this->bNetAddressable || Super::IsNameStableForNetworking();
}

bool URepWidget::IsSupportedForNetworking() const
{
    return true;
}

int32 URepWidget::GetFunctionCallspace(UFunction* Function, FFrame* Stack)
{
    // Bubble up to outer (must be an actor/component/repobject)
    return this->GetOuter() != nullptr
        ? this->GetOuter()->GetFunctionCallspace(Function, Stack)
        : FunctionCallspace::Local;
}

bool URepWidget::CallRemoteFunction(UFunction* Function, void* Params, FOutParmRec* OutParams, FFrame* Stack)
{
    for (UObject* Outer = this->GetOuter(); Outer != nullptr; Outer = Outer->GetOuter())
    {
        if (AActor* const OuterActor = Cast<AActor>(Outer))
        {
            if (UNetDriver* const NetDriver = OuterActor->GetNetDriver())
            {
                NetDriver->ProcessRemoteFunction(OuterActor, Function, Params, OutParams, Stack, this);

                return true;
            }

            break;
        }
    }

    return false;
}

URepWidget* URepWidget::CreateReplicatedWidget(APlayerController* Player, TSubclassOf<URepWidget> Class, FName Name, bool bMakeNetAddressable)
{
    if (Class == nullptr)
    {
        return nullptr;
    }

    URepWidget* NewWidget = nullptr;
    if (Player->IsLocalController())
    {
        NewWidget = CreateWidget<URepWidget>(Player, Class, Name);
        NewWidget->Rename(nullptr, Player);
    }
    else
    {
        NewWidget = NewObject<URepWidget>(Player, Class, Name);
        NewWidget->Initialize();
    }

    NewWidget->bNetAddressable = bMakeNetAddressable;

    Player->AddReplicatedSubObject(NewWidget);

    return NewWidget;
}
